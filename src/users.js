function init(DB, app) {

    console.log('Setting up questions!!');
    app.get('/Users/:id', function (req, res) {

        let id = req.params.id;
        let userPromise = DB.ref(`/Users/${id}`).once('value');

        userPromise.then((snapshot) => {
            res.send(snapshot.val());
        });

    });


    app.put('/Users/:id', function (req, res) {

        let id = req.params.id;
        let body = req.body;
        let userPromise = DB.ref(`/Users/${id}`).update(body);

        userPromise.then(() => {
            res.send('Usuario actualizado');
        });

    });

    app.post('/Users', function (req, res) {

        let body = req.body;
        let userPromise = DB.ref(`/Users`).push(body);

        userPromise.then(function (ref) {
            return ref.once('value');
        }).then((snapshot) => {
            let val = snapshot.val();
            val.id = snapshot.key;

            res.send(val);
        });

    });

    app.delete('/Users', function (req, res) {

        let id = req.params.id;


    });
    //
    function arrayJson(id, questions) {
        var Datos =
        {
            Id: id,
            questions: questions,
        }

        return Datos;
    }

}

module.exports = init;
