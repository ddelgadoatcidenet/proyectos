
//firebase.database.Reference;
const express = require('express');
const cors = require('cors');
const app = express();

// app. = esxpress -  http
app.use(cors({ origin: true }));
var bodyParser = require('body-parser');
var firebase = require('firebase');
app.use(bodyParser.json()); 

// Initializa Firebase
var config = {
    apiKey: "AIzaSyDqi6TpRcYAvEXqucThVCNJxLMs3wHH7jA",
    authDomain: "example1-a3fc8.firebaseapp.com",
    databaseURL: "https://example1-a3fc8.firebaseio.com",
    projectId: "example1-a3fc8",
    storageBucket: "example1-a3fc8.appspot.com",
    messagingSenderId: "541062882512"
};
firebase.initializeApp(config);

var database = firebase.database();

const users = require('./src/users');
users(database, app);

var server = app.listen(3000, function(){
	var host=server.address().address;
	var port=server.address().port;
	console.log(`Estas en el host ${host} y en el puerto ${port}`); 
});
